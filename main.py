from itertools import count
from pickle import EMPTY_DICT, TRUE
import translate
from wikipedia.wikipedia import languages
from Jarvis import JarvisAssistant
import re
import os
import random
import pprint
import datetime
import requests
import sys
from deep_translator import GoogleTranslator
import imaplib
import email
# from googletrans import Translator
import gtts
from playsound import playsound
import urllib.parse
import pyjokes
import time
import pyautogui
import pywhatkit
import json
import wolframalpha
import smtplib
import datetime as dt
from PIL import Image
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import QTimer, QTime, QDate, Qt
from PyQt5.QtGui import QMovie
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUiType
import Jarvis
from Jarvis.features import voice_training
from Jarvis.features.gui import VideoPlayer
from Jarvis.config import config
import schedule
from pynput.keyboard import Key, Controller
import pywhatkit as kit
import threading
from plyer import notification
import email_listener
from win10toast import ToastNotifier
import subprocess
import sounddevice as sd
from scipy.io.wavfile import write
import numpy as np
from sklearn import preprocessing
import python_speech_features as mfcc
import pickle
from scipy.io.wavfile import read
from sklearn.mixture import GaussianMixture as GMM
import warnings


warnings.filterwarnings("ignore")


obj = JarvisAssistant()

# ================================ MEMORY ===========================================================================================================

GREETINGS = ["hello jarvis", "jarvis", "wake up jarvis", "you there jarvis", "time to work jarvis", "hey jarvis",
             "ok jarvis", "are you there"]
GREETINGS_RES = ["always there for you sir", "i am ready sir",
                 "your wish my command", "how can i help you sir?", "i am online and ready sir"]

EMAIL_DIC = {
    'myself': 'ABC@gmail.com',
    'my official email': 'ABC@gmail.com',
    'my second email': 'ABC@gmail.com',
    'my official mail': 'ABC@gmail.com',
    'my second mail': 'ABC@gmail.com'
}

CALENDAR_STRS = ["what do i have", "do i have plans", "am i busy"]
# =======================================================================================================================================================


def speak(text):
    obj.tts(text)


app_id = config.wolframalpha_id


def computational_intelligence(question):
    try:
        client = wolframalpha.Client(app_id)
        answer = client.query(question)
        answer = next(answer.results).text
        print(answer)
        return answer
    except:
        speak("Sorry sir I couldn't fetch your question's answer. Please try again ")
        return None


def startup():
    
    #getting name
    #create required files
    #open file for reading
   
    try:
        if os.path.exists("Jarvis/config/assistantName.txt"):
            with open('Jarvis/config/assistantName.txt') as f:
                data = f.read()
            js =json.loads(data)
            if js["name"]:
                name=js["name"]
                # print("name is not empty")
            else:
                speak("Welcome Sir")
                speak("I am your AI assistant, please let me know my name")
                name=obj.mic_input()
                name_details = {'name': name}
                with open('Jarvis/config/assistantName.txt', 'w') as write_file:
                    write_file.write(json.dumps(name_details))
                print(name_details) 
        else:
            open('Jarvis/config/assistantName.txt', 'w')
            speak("Welcome Sir")
            speak("I am your AI assistant, please let me know my name")
            name=obj.mic_input()
            name_details = {'name': name}
            with open('Jarvis/config/assistantName.txt', 'w') as write_file:
                write_file.write(json.dumps(name_details))
            print(name_details) 
            
    except Exception as e:
        print(e)    
    
      
    speak(f"Initializing {name}")
    speak("Starting all systems services")
    #speak("Working on setting up the things")
    # # speak("Checking the internet connection")
    # speak("Please wait sir")
    # # speak("Internet connection is good")
    # speak("All systems have been activated")
    # hour = int(datetime.datetime.now().hour)
    # if hour >= 0 and hour <= 12:
    #     speak("Good Morning")
    # elif hour > 12 and hour < 18:
    #     speak("Good afternoon")
    # else:
    #     speak("Good evening")
    c_time = obj.tell_time()
    # speak(f"Currently it is {c_time}")
    # speak(f"I am {name}. Online and ready sir. Please tell me how may I help you")


def wish():
    hour = int(datetime.datetime.now().hour)
    if hour >= 0 and hour <= 12:
        speak("Good Morning")
    elif hour > 12 and hour < 18:
        speak("Good afternoon")
    else:
        speak("Good evening")
    c_time = obj.tell_time()
    if not os.path.exists("data"):
        os.makedirs("data")     
    if not os.path.exists("models"):
        os.makedirs("models")  
  
   # speak("Currently it is {c_time}")
   # speak("I am Jarvis. Online and ready sir. Please tell me how may I help you")
# if __name__ == "__main__":

def calculate_delta(array):
        """Calculate and returns the delta of given feature vector matrix"""

        rows,cols = array.shape
        deltas = np.zeros((rows,20))
        N = 2
        for i in range(rows):
            index = []
            j = 1
            while j <= N:
                if i-j < 0:                 
                    first = 0             
                else:                 
                    first = i-j             
                
                if i+j > rows-1:
                    second = rows-1
                else:
                    second = i+j 
                
                index.append((second,first))
                j+=1
            deltas[i] = ( array[index[0][0]]-array[index[0][1]] + (2 * (array[index[1][0]]-array[index[1][1]])) ) / 10
        return deltas
def extract_features(audio,rate):
    """extract 20 dim mfcc features from an audio, performs CMS and combines 
    delta to make it 40 dim feature vector"""    
    
    mfcc_feat = mfcc.mfcc(audio,rate, 0.025, 0.01,20,26,512,appendEnergy = True)    
    mfcc_feat = preprocessing.scale(mfcc_feat)
    
    delta = calculate_delta(mfcc_feat)
    combined = np.hstack((mfcc_feat,delta)) 
    return combined

    
def train_voice_model():
    
      #path to training data
    
    source   = "data\\"   
    #path where training speakers will be saved
    dest = "models\\"
    train_file = "training_list.txt"        
    file_paths = open(train_file,'r')
    count = 1
    # Extracting features for each digit data provided by speaker 
    features = np.asarray(())
    for path in file_paths:    
        path = path.strip()   
        print(path)
        
        # read the audio
        sr,audio = read(source + path)
        
        # extract 40 dimensional MFCC & delta MFCC features
        vector   = extract_features(audio,sr)
        
        if features.size == 0:
            features = vector
        else:
            features = np.vstack((features, vector))
        # when features of training files of each digit for a given speaker are concatenated, model training is done
        if count == 4:    
            gmm = GMM(n_components = 16,  max_iter= 200, covariance_type='diag',n_init = 3)
            gmm.fit(features)
            
            # dumping the trained gaussian model
            picklefile = path.split("\\")[0]+".gmm"
            pickle.dump(gmm,open(dest + picklefile,'wb'))
            print('+ modeling completed for speaker:',picklefile," with data point = ",features.shape)    
            features = np.asarray(())
            count = 0
        count = count + 1

def check_user_voice():
    #path to training data
    source   = "data\\"   
    models_dir = "models\\"
    test_file = "testing_list.txt"        
    file_paths = open(test_file,'r')
    gmm_files = [os.path.join(models_dir,fname) for fname in os.listdir(models_dir) if fname.endswith('.gmm')]
    gmm_files
    #Load the Gaussian Models
    models = [pickle.load(open(fname,'rb')) for fname in gmm_files]
    models
    speakers_digit = [fname.split("\\")[-1].split(".gmm")[0] for fname in gmm_files]
    speakers_digit
    # Read the test directory and get the list of test audio files 
    path_predicted_dict = dict()

    for path in file_paths:   
        
        path = path.strip()   
        print(path)
        sr,audio = read(path)
        vector   = extract_features(audio,sr)
        
        log_likelihood = np.zeros(len(models)) 
        
        for i in range(len(models)):
            gmm    = models[i]  #checking with each model one by one
            scores = np.array(gmm.score(vector))
            log_likelihood[i] = scores.sum()
        
        winner = np.argmax(log_likelihood)
        print("\tSpeaker & Digit detected as - ", speakers_digit[winner])
        speak("Hello"+speakers_digit[winner]+"I hope you are doing good")
        path_predicted_dict[path] = speakers_digit[winner]
        
    correct_pred = 0
    incorrect_pred = 0
    for i in path_predicted_dict.keys():
        if i.split("\\")[0] == path_predicted_dict[i]:
            correct_pred += 1
        else:
            incorrect_pred += 1

    # print("Corrected Predictions are:",correct_pred)
    # print("InCorrect Predictions are:",incorrect_pred)
    # print("Accuracy on Test Dataset = ", correct_pred*100/(correct_pred + incorrect_pred), "%")


#starting new email looking service
class MailCheckThread(threading.Thread):
    def run(self):
        speak("Email listener for new mails has been started.")
        speak("Please tell me how may I help you")
        def listen_for_email():
            user = config.email
            password = config.email_password
            folder = 'Inbox'
            # unread emails would be saved here
            attachment_dir = "Jarvis/config/attachments"
            if not os.path.exists(attachment_dir):
                os.makedirs(attachment_dir)
            con = email_listener.EmailListener(user, password, folder,attachment_dir)
            con.login()
            # read the currently unread email in the FOLDER
            messages = con.scrape()
            toast = ToastNotifier()
             
            if len(messages)==0:
                print("No new email")
            else:
                toast.show_toast("Email","New email received kindly check inbox",icon_path=None,duration=5,threaded=True)
                speak("New email received kindly check inbox")
                # print(messages)
        schedule.every(5).minutes.do(listen_for_email)    
        while True:
            schedule.run_pending()

        
class MainThread(QThread):
    def __init__(self):
        super(MainThread, self).__init__()

    def run(self):
        self.TaskExecution()

    def TaskExecution(self):
        startup()
        wish()
        MailThread=MailCheckThread()
        MailThread.start()

        while True:
            command = obj.mic_input()

            if re.search('date', command):
                date = obj.tell_me_date()
                print(date)
                speak(date)

            elif "time" in command or "what is the time" in command:
                time_c = obj.tell_time()
                print(time_c)
                speak(f"Sir the time is {time_c}")

            elif re.search('launch', command):
                dict_app = {
                    'chrome': 'C:/Program Files/Google/Chrome/Application/chrome'
                }

                app = command.split(' ', 1)[1]
                path = dict_app.get(app)

                if path is None:
                    speak('Application path not found')
                    print('Application path not found')

                else:
                    speak('Launching: ' + app + 'for you sir!')
                    obj.launch_any_app(path_of_app=path)

            elif command in GREETINGS:
                speak(random.choice(GREETINGS_RES))

            elif re.search('open', command):
                domain = command.split(' ')[-1]
                open_result = obj.website_opener(domain)
                speak(f'Alright sir !! Opening {domain}')
                print(open_result)

            elif re.search('weather', command):
                city = command.split(' ')[-1]
                weather_res = obj.weather(city=city)
                print(weather_res)
                speak(weather_res)

            elif re.search('tell me about', command):
                topic = command.split(' ')[-1]
                if topic:
                    wiki_res = obj.tell_me(topic)
                    print(wiki_res)
                    speak(wiki_res)
                else:
                    speak(
                        "Sorry sir. I couldn't load your query from my database. Please try again")

            elif "buzzing" in command or "tell me news" in command or "headlines" in command:
                news_res = obj.news()
                speak(f'Source: The Times Of U S')
                speak(f'Todays Headlines are..')
                count=0
                for index, articles in enumerate(news_res):
                    pprint.pprint(articles['title'])
                    speak(articles['title'])
                    count=count+1
                    if count>2:
                        break
                speak('These were the top headlines, Have a nice day Sir!!..')

            elif 'search google for' in command:
                obj.search_anything_google(command)

            elif "play music" in command or "hit some music" in command:
                music_dir = "F://Songs//Imagine_Dragons"
                songs = os.listdir(music_dir)
                for song in songs:
                    os.startfile(os.path.join(music_dir, song))

            elif 'youtube' in command:
                video = command.split(' ')[1]
                speak(f"Okay sir, playing {video} on youtube")
                pywhatkit.playonyt(video)
                
            #email    
            elif "email" in command or "send email" in command:
                sender_email = config.email
                sender_password = config.email_password
                try:
                    speak("Whom do you want to email sir ?")
                    # reading the data from the file
                    with open('Jarvis/config/emaildictionary.txt') as f:
                        data = f.read()
                    js = json.loads(data)
                    recipient = obj.mic_input()
                    receiver_email = js[recipient]
                    if receiver_email:
                        speak("What is the subject sir ?")
                        subject = obj.mic_input()
                        speak("What should I say?")
                        message = obj.mic_input()
                        msg = 'Subject: {}\n\n{}'.format(subject, message)
                        obj.send_mail(sender_email, sender_password,
                                      receiver_email, msg)
                        speak("Email has been successfully sent")
                        time.sleep(2)
                    else:
                        speak(
                            "I coudn't find the requested person's email in my database. Please try again with a different name")

                except:
                    speak("Sorry sir. Couldn't send your mail. Please try again")

            #Schedule email
            elif "schedule email" in command or "send schedule email" in command:
                sender_email = config.email
                sender_password = config.email_password
                try:
                    # reading the data from the file
                    with open('Jarvis/config/emaildictionary.txt') as f:
                        data = f.read()
                    js = json.loads(data)
                    speak("Whom do you want to email sir ?")
                    recipient = obj.mic_input()
                    receiver_email = js[recipient]
                    # print(js[recipient])
                    class BackgroundTasks(threading.Thread):
                        def run(self,*args,**kwargs):
                            def send_email():
                                    server = smtplib.SMTP('smtp.gmail.com', 587)
                                    server.starttls()
                                    server.login(sender_email, sender_password)
                                    server.sendmail(sender_email, receiver_email, msg)
                                    server.quit()
                                    return schedule.CancelJob
                            schedule.every(int(sending_hour)).hour.do(send_email)
                            # schedule.every().day.at("06:18PM").do(send_email)
                            speak('Your email will be sent after'+sending_hour+'hour')
                            time.sleep(1)
                            speak('let me know what else i can help you with today')
                            while True:
                                schedule.run_pending()
                                if not schedule.jobs:
                                    break
                                time.sleep(1)
                            print("Email Sent")
                        
                    t=BackgroundTasks()
                    speak("sending schedule email sir.")
                    speak("What is the subject sir ?")
                    subject = obj.mic_input()
                    speak("What should I say?")
                    message = obj.mic_input()
                    msg = 'Subject: {}\n\n{}'.format(subject, message)
                    speak('after how many hours you want to send this email sir?')
                    sending_hour=obj.mic_input()
                    t.start()

                except Exception as e:
                    speak("Sorry sir. Couldn't send your mail. Please try again")
                    print(e)
                                   
            #Sending Whatapp messages
            elif "whatsapp" in command or "send whatsapp message" in command or "send whatsap" in command:
                try:
                    class BackgroundTasks_whatsapp(threading.Thread):
                        def run(self,*args,**kwargs):
                                kit.sendwhatmsg(reciver_number, whatsapp_message, sending_time, 00)
                        
                    speak("scheduling whatsapp message sir")
                    speak("Whom do you want to message sir ?")
                    reciver_name=obj.mic_input()
                    with open('Jarvis/config/whatsappdictionary.txt') as f:
                        data = f.read()
                    js = json.loads(data)
                    reciver_number=js[reciver_name]
                    print(reciver_number)
                    speak('what is the message sir?')
                    whatsapp_message=obj.mic_input()
                    speak('After how many hours you want to send this message?')
                    current_time=datetime.datetime.now().strftime('%I')
                    sending_hour=obj.mic_input()
                    print("current hour is"+current_time)
                    print("sendiong hours is "+sending_hour)
                    sending_time=int(current_time)+int(sending_hour)
                    print(sending_time)
                    t=BackgroundTasks_whatsapp()
                    t.start()  
                    
                except Exception as e:
                        print(e)
                        speak("Sorry sir. Couldn't send your message. Please try again")
                            
            elif "reminder" in command or "set alarm" in command:
                # try:
                class setReminder(threading.Thread):
                    def run(self):
                        def reminder():
                            speak("Reminder")
                            speak(label)
                            time.sleep(5)
                            speak(label)
                            time.sleep(5)
                            speak(label)
                            time.sleep(5)
                        schedule.every(int(hours)).hour.do(reminder)
                        speak(f"Your reminder have been set Sir , i will remind you after {hours} hours")
                        while True:
                                schedule.run_pending()
                                if not schedule.jobs:
                                    break
                                time.sleep(1)                                                     
                speak("What will be the label of the reminder")
                label=obj.mic_input()
                speak("After how many hours i should remind you sir?")
                hours=obj.mic_input()
                t1=setReminder()
                t1.start() 
                # except Exception as e:
                #     speak("Sorry sir. Couldn't set your alarm. Please try again")
                    
            elif "what is" in command or "who is" in command:
                question = command
                answer = computational_intelligence(question)
                speak(answer)

            # elif "what do i have" in command or "do i have plans" or "am i busy" in command:
            #     obj.google_calendar_events(command)

            elif "make a note" in command or "write this down" in command or "remember this" in command:
                speak("What would you like me to write down?")
                note_text = obj.mic_input()
                obj.take_note(note_text)
                speak("I've made a note of that")

            elif "close the note" in command or "close notepad" in command:
                speak("Okay sir, closing notepad")
                os.system("taskkill /f /im notepad.exe")

            elif "joke" in command:
                joke = pyjokes.get_joke()
                print(joke)
                speak(joke)

            elif "system" in command:
                sys_info = obj.system_info()
                print(sys_info)
                speak(sys_info)

            elif "where is" in command:
                place = command.split('where is ', 1)[1]
                current_loc, target_loc, distance = obj.location(place)
                city = target_loc.get('city', '')
                state = target_loc.get('state', '')
                country = target_loc.get('country', '')
                time.sleep(1)
                try:

                    if city:
                        res = f"{place} is in {state} state and country {country}. It is {distance} km away from your current location"
                        print(res)
                        speak(res)

                    else:
                        res = f"{state} is a state in {country}. It is {distance} km away from your current location"
                        print(res)
                        speak(res)

                except:
                    res = "Sorry sir, I couldn't get the co-ordinates of the location you requested. Please try again"
                    speak(res)

            elif "ip address" in command:
                ip = requests.get('https://api.ipify.org').text
                time.sleep(1)
                print(ip)
                speak("Your ip address is"+ip)

            elif "switch the window" in command or "switch window" in command:
                speak("Okay sir, Switching the window")
                pyautogui.keyDown("alt")
                pyautogui.press("tab")
                time.sleep(1)
                pyautogui.keyUp("alt")

            elif "where i am" in command or "current location" in command or "where am i" in command:
                try:
                    city, state, country = obj.my_location()
                    print(city, state, country)
                    speak(f"You are currently in {city} city which is in {state} state and country {country}")
                except Exception as e:
                    speak(
                        "Sorry sir, I coundn't fetch your current location. Please try again")

            elif "take screenshot" in command or "take a screenshot" in command or "capture the screen" in command:
                speak("By what name do you want to save the screenshot?")
                name = obj.mic_input()
                speak("Alright sir, taking the screenshot")
                img = pyautogui.screenshot()
                name = f"{name}.png"
                img.save(name)
                speak("The screenshot has been succesfully captured")

            elif "show me the screenshot" in command:
                try:
                    img = Image.open('D://JARVIS//JARVIS_2.0//' + name)
                    img.show(img)
                    speak("Here it is sir")
                    time.sleep(2)

                except IOError:
                    speak("Sorry sir, I am unable to display the screenshot")

            elif "hide all files" in command or "hide this folder" in command:
                os.system("attrib +h /s /d")
                speak("Sir, all the files in this folder are now hidden")

            elif "visible" in command or "make files visible" in command:
                os.system("attrib -h /s /d")
                speak("Sir, all the files in this folder are now visible to everyone. I hope you are taking this decision in your own peace")

            elif "calculate" in command or "what is" in command:
                query = command
                answer = computational_intelligence(query)
                speak(answer)
            
            elif "virtual keyboard" in command:
                def keyboardWriter():
                    text=obj.mic_input()
                    if "disable virtual keyboard" == text:
                        speak("virtual keyboard disabled Sir.")
                        speak("let me know what else i can help you with")
                        return True
                    else:
                        keyboard = Controller()
                        keyboard.type(text)
                        return False
                speak("Now you can type whatever you want by speaking Sir..")
                while True:
                    if(keyboardWriter()):
                        break
                       
            elif re.search('translate to', command):
                
                language = command.split(' ')[-1]
                print(language)
                speak("what do you want to translate sir?")
                non_translated_text=obj.mic_input()
                langs_dict = GoogleTranslator.get_supported_languages(as_dict=True)
                translated = GoogleTranslator(source='auto', target=langs_dict[language]).translate(non_translated_text)
                time.sleep(1)
                # tts = gtts.gTTS(translated)
                # tts.save("hello.mp3")
                # playsound("hello.mp3")
                print (translated)
                speak(translated)
            elif "rename" in command or "rename" in command:
                speak("What do you want to call me Sir?")
                new_name=obj.mic_input()
                new_name_details = {'name': new_name}
                with open('Jarvis/config/assistantName.txt', 'w') as write_file:
                    write_file.write(json.dumps(new_name_details))
                speak(f"OK Sir now you can call me {new_name}")
                
            
            elif "train my voice" in command or "voice training" in command:
                speak("Please tell me your name Sir?")
                speaker_name=obj.mic_input()
                time.sleep(0)
                speak("Please record your four voice recordings and in recording say something so i can remember you by your voice")
                directory_name=speaker_name
                if not os.path.exists("data/"+directory_name+"/wav"):
                    os.makedirs("data/"+directory_name+"/wav")     
                
                counter=0
                while(counter<4):
                    counter=counter+1
                    fs = 44100  # Sample rate
                    seconds = 4  # Duration of recording
                    print("Recording...")
                    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
                    sd.wait()  # Wait until recording is finished
                    timestemp = time.strftime("%Y%m%d-%H%M%S")
                    write("data/"+directory_name+"/wav/"+directory_name+"_"+timestemp+".wav", fs, myrecording)  # Save as WAV file 
                    
                    f = open("training_list.txt", "a")
                    if os.path.getsize("training_list.txt") == 0:
                        f.write(directory_name+"\\wav\\"+directory_name+"_"+timestemp+".wav")
                        f.close()
                        print("Recording saved successfully") 
                    else:
                        f.write("\n"+directory_name+"\\wav\\"+directory_name+"_"+timestemp+".wav")
                        f.close()
                        print("Recording saved successfully") 
                    
                time.sleep(1)
                speak("Now creating voice model for your identification Sir")
                train_voice_model()
                speak("Voice Model trained successfully, now i will be able to recognise you by your voice Sir")
                
                
            elif "recognise me" in command:
                speak("Sure Sir!")
                speak("Say who am i")
                fs = 44100  # Sample rate
                seconds = 4  # Duration of recording
                print("Recording...")
                myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
                sd.wait()  # Wait until recording is finished
                timestemp = time.strftime("%Y%m%d-%H%M%S")
                write("data/"+"testRecording_"+timestemp+".wav", fs, myrecording)  # Save as WAV file 
                f = open("testing_list.txt", "a")
                if os.path.getsize("testing_list.txt") == 0:
                    f.write("data\\"+"testRecording"+"_"+timestemp+".wav")
                    f.close()
                    print("Recording saved successfully") 
                else:
                    f.write("\n"+"data\\"+"testRecording"+"_"+timestemp+".wav")
                    f.close()
                    print("Recording saved successfully")
                speak("Please wait i am recognising your voice")
                check_user_voice()
            
            elif "goodbye" in command or "offline" in command or "bye" in command:
                speak("Alright sir, going offline. It was nice working with you")
                try:
                    sys.exit()
                except Exception as e:
                    print("")



startExecution = MainThread()


class Main(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = VideoPlayer()
        self.ui.resize(620, 540)
        self.ui.show()
        # self.ui.setupUi(self)
        # self.ui.pushButton.clicked.connect(self.startTask)
        # self.ui.pushButton_2.clicked.connect(self.close)
        self.startTask()

    # def __del__(self):
    #     sys.stdout = sys.__stdout__

    # def run(self):
    #     self.TaskExection
    def startTask(self):
        timer = QTimer(self)
        timer.timeout.connect(self.showTime)
        timer.start(1000)
        startExecution.start()

    def showTime(self):
        current_time = QTime.currentTime()
        current_date = QDate.currentDate()
        label_time = current_time.toString('hh:mm:ss')
        label_date = current_date.toString(Qt.ISODate)
        # self.ui.textBrowser.setText(label_date)
        # self.ui.textBrowser_2.setText(label_time)


app = QApplication(sys.argv)
jarvis = Main()
jarvis.show()
jarvis.hide()
sys.exit(app.exec_())