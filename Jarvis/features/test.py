import os
import sounddevice as sd
from scipy.io.wavfile import write
import time


directory_name="ali"
if not os.path.exists("data/"+directory_name+"/wav"):
    os.makedirs("data/"+directory_name+"/wav")     
counter=0
while(counter<4):
    counter=counter+1
    print(counter)
    fs = 44100  # Sample rate
    seconds = 4  # Duration of recording
    print("Recording...")
    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
    sd.wait()  # Wait until recording is finished
    timestemp = time.strftime("%Y%m%d-%H%M%S")
    write("data/"+directory_name+"/wav/"+directory_name+"_"+timestemp+".wav", fs, myrecording)  # Save as WAV file 
    f = open("training_list.txt", "a")
    f.write("\n"+directory_name+"\\wav\\"+directory_name+"_"+timestemp+".wav")
    f.close()
    print("Recording saved successfully") 