import requests
import json



def get_news():
    url = 'http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=edc261d6bac54b25b900dc825c498dc2'
    news = requests.get(url).text
    news_dict = json.loads(news)
    articles = news_dict['articles']
    try:
        return articles
    except:
        return False


def getNewsUrl():
    return 'http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=edc261d6bac54b25b900dc825c498dc2'
