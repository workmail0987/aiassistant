# import numpy as np
# from sklearn import preprocessing
# import python_speech_features as mfcc
# import pickle
# import numpy as np
# from scipy.io.wavfile import read
# from sklearn.mixture import GaussianMixture as GMM
# import warnings
# warnings.filterwarnings("ignore")
# import time, os


# #Helper functions to calculate Deltas and Extract Features


# class voice_training:
#     def __init__(self) -> None:
#         pass
            
#     def calculate_delta(array):
#         """Calculate and returns the delta of given feature vector matrix"""

#         rows,cols = array.shape
#         deltas = np.zeros((rows,20))
#         N = 2
#         for i in range(rows):
#             index = []
#             j = 1
#             while j <= N:
#                 if i-j < 0:                 
#                     first = 0             
#                 else:                 
#                     first = i-j             
                
#                 if i+j > rows-1:
#                     second = rows-1
#                 else:
#                     second = i+j 
                
#                 index.append((second,first))
#                 j+=1
#             deltas[i] = ( array[index[0][0]]-array[index[0][1]] + (2 * (array[index[1][0]]-array[index[1][1]])) ) / 10
#         return deltas



# def extract_features(audio,rate):
#     """extract 20 dim mfcc features from an audio, performs CMS and combines 
#     delta to make it 40 dim feature vector"""    
    
#     mfcc_feat = mfcc.mfcc(audio,rate, 0.025, 0.01,20,26,1200,appendEnergy = True)    
#     mfcc_feat = preprocessing.scale(mfcc_feat)
    
#     delta = voice_training.calculate_delta(mfcc_feat)
#     combined = np.hstack((mfcc_feat,delta)) 
#     return combined



# # if __name__ == '__main__':
    
#     #path to training data
#     source   = "data\\"   
#     #path where training speakers will be saved
#     dest = "models\\"
#     train_file = "training_list.txt"        
#     file_paths = open(train_file,'r')

#     count = 1
#     # Extracting features for each digit data provided by speaker 
#     features = np.asarray(())
#     for path in file_paths:    
#         path = path.strip()   
#         print(path)
        
#         # read the audio
#         sr,audio = read(source + path)
        
#         # extract 40 dimensional MFCC & delta MFCC features
#         vector   = extract_features(audio,sr)
        
#         if features.size == 0:
#             features = vector
#         else:
#             features = np.vstack((features, vector))
#         # when features of training files of each digit for a given speaker are concatenated, model training is done
#         if count == 4:    
#             gmm = GMM(n_components = 16,  max_iter= 200, covariance_type='diag',n_init = 3)
#             gmm.fit(features)
            
#             # dumping the trained gaussian model
#             picklefile = path.split("\\")[0]+".gmm"
#             pickle.dump(gmm,open(dest + picklefile,'wb'))
#             print('+ modeling completed for speaker:',picklefile," with data point = ",features.shape)    
#             features = np.asarray(())
#             count = 0
#         count = count + 1




        # #path to training data
        # source   = "data\\"   
        # models_dir = "models\\"
        # test_file = "testing_list.txt"        
        # file_paths = open(test_file,'r')

        # gmm_files = [os.path.join(models_dir,fname) for fname in os.listdir(models_dir) if fname.endswith('.gmm')]

        # gmm_files

        # #Load the Gaussian Models
        # models    = [pickle.load(open(fname,'rb')) for fname in gmm_files]
        # models
        # speakers_digit   = [fname.split("\\")[-1].split(".gmm")[0] for fname in gmm_files]
        # speakers_digit
        # # Read the test directory and get the list of test audio files 
        # path_predicted_dict = dict()

        # for path in file_paths:   
            
        #     path = path.strip()   
        #     print(path)
        #     sr,audio = read(source + path)
        #     vector   = extract_features(audio,sr)
            
        #     log_likelihood = np.zeros(len(models)) 
            
        #     for i in range(len(models)):
        #         gmm    = models[i]  #checking with each model one by one
        #         scores = np.array(gmm.score(vector))
        #         log_likelihood[i] = scores.sum()
            
        #     winner = np.argmax(log_likelihood)
        #     print("\tSpeaker & Digit detected as - ", speakers_digit[winner])
        #     path_predicted_dict[path] = speakers_digit[winner]
            
        # correct_pred = 0
        # incorrect_pred = 0
        # for i in path_predicted_dict.keys():
        #     if i.split("\\")[0] == path_predicted_dict[i]:
        #         correct_pred += 1
        #     else:
        #         incorrect_pred += 1

        # print("Corrected Predictions are:",correct_pred)
        # print("InCorrect Predictions are:",incorrect_pred)
        # print("Accuracy on Test Dataset = ", correct_pred*100/(correct_pred + incorrect_pred), "%")